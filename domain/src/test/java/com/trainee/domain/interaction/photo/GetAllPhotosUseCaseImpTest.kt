package com.trainee.domain.interaction.photo

import com.trainee.domain.data.source.FakePhotoRepository
import com.trainee.domain.model.photo.Photo
import com.trainee.domain.model.photo.succeeded
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.hamcrest.Matchers.*
import org.junit.Test
import org.koin.core.component.getScopeId

@ExperimentalCoroutinesApi
class GetAllPhotosUseCaseImpTest {

    @Test
    fun getAllPhotosUseCase_notNull_returnSuccessTrueAndNotNullList() = runBlockingTest {

        // Given a list of photo
        val photo1 = Photo(id = 1, 1, "Title1", url = "url1", thumbnailUrl = "turl1")
        val photo2 = Photo(id = 2, 1, "Title2", url = "url2", thumbnailUrl = "turl2")
        val photo3 = Photo(id = 3, 1, "Title3", url = "url3", thumbnailUrl = "turl3")
        val photoListFromRepository = listOf(photo1, photo2, photo3).sortedBy { it.id }

        // When requests photos
        val fakePhotoRepository = FakePhotoRepository(photoListFromRepository.toMutableList())
        val getAllPhotosUseCaseImp = GetAllPhotosUseCaseImp(fakePhotoRepository)

        val result = getAllPhotosUseCaseImp.invoke()

        assertThat(result.succeeded, `is`(true))
    }
}