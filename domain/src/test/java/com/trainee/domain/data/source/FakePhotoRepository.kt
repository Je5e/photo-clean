package com.trainee.domain.data.source

import com.trainee.domain.model.photo.Photo
import com.trainee.domain.model.photo.Result
import com.trainee.domain.repository.PhotoRepository
import java.lang.Error
import java.lang.Exception

class FakePhotoRepository(val photos: MutableList<Photo>? = mutableListOf()) : PhotoRepository {

    override suspend fun getAllPhotos(): Result<List<Photo>> {
        photos?.let { return Result.Success(it) }
        return Result.Error(Exception("Photos not found"))
    }

    override suspend fun refreshPhotos() {
        TODO("Not yet implemented")
    }
}