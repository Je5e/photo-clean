package com.trainee.domain.di

import com.trainee.domain.interaction.photo.GetAllPhotosUseCase
import com.trainee.domain.interaction.photo.GetAllPhotosUseCaseImp
import com.trainee.domain.interaction.photo.RefreshPhotosUseCase
import com.trainee.domain.interaction.photo.RefreshPhotosUseCaseImp
import org.koin.dsl.module

val interactionModule = module {
    factory<GetAllPhotosUseCase> { GetAllPhotosUseCaseImp(get()) }
    factory<RefreshPhotosUseCase>{RefreshPhotosUseCaseImp (get())}
}