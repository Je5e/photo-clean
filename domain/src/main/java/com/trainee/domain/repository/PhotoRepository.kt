package com.trainee.domain.repository

import com.trainee.domain.model.photo.Photo
import com.trainee.domain.model.photo.Result

interface PhotoRepository {
    suspend fun getAllPhotos(): Result<List<Photo>> // MutableLiveEvent

    suspend fun refreshPhotos()
}