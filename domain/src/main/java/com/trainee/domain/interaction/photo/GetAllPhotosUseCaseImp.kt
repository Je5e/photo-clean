package com.trainee.domain.interaction.photo

import com.trainee.domain.repository.PhotoRepository

class GetAllPhotosUseCaseImp(private val repository: PhotoRepository) : GetAllPhotosUseCase {
    override suspend fun invoke() = repository.getAllPhotos()
}