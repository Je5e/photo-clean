package com.trainee.domain.interaction.photo

import androidx.lifecycle.LiveData
import com.trainee.domain.model.photo.Photo
import com.trainee.domain.model.photo.Result

interface GetAllPhotosUseCase {
    suspend operator fun invoke(): Result<List<Photo>>
}