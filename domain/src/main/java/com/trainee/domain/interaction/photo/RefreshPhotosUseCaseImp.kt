package com.trainee.domain.interaction.photo

import com.trainee.domain.repository.PhotoRepository

class RefreshPhotosUseCaseImp(private val repository: PhotoRepository) : RefreshPhotosUseCase {
    override suspend fun invoke() = repository.refreshPhotos()
}