package com.trainee.data.di

import com.trainee.data.repository.PhotoRepositoryImp
import com.trainee.domain.repository.PhotoRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<PhotoRepository> { PhotoRepositoryImp(get(), get()) }

}