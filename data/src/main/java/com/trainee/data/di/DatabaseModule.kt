package com.trainee.data.di

import androidx.room.Room
import com.trainee.data.database.PhotoDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

private const val PHOTO_DB = "photo-database"

val databaseModule = module {
    single {
        Room.databaseBuilder(androidContext(), PhotoDatabase::class.java, PHOTO_DB).fallbackToDestructiveMigration().build()
    }
    factory { get<PhotoDatabase>().photoDao }
}