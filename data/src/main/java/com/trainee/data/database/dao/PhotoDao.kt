package com.trainee.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.trainee.data.database.model.PhotoEntity
import com.trainee.domain.model.photo.Result

@Dao
interface PhotoDao {
    @Query("SELECT * FROM PhotoEntity")
    suspend fun getPhotos(): List<PhotoEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll( photos: List<PhotoEntity>)
}