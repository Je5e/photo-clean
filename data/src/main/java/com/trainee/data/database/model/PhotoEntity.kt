package com.trainee.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.trainee.domain.model.photo.Photo

@Entity
class PhotoEntity(
    @PrimaryKey
    val id: Int,
    val albumId: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
) {
    fun asDomainModel(): Photo {
        return (
            Photo(
                id = id,
                albumId = albumId,
                title = title,
                url = url,
                thumbnailUrl = thumbnailUrl
            )
        )
        /*
        return  Result(
            Photo(
                id = it.id,
                albumId = it.albumId,
                title = it.title,
                url = it.url,
                thumbnailUrl = it.thumbnailUrl
            )
        )*/

    }
}

