package com.trainee.data.networking

import com.trainee.data.networking.model.PhotoResponse
import com.trainee.domain.model.photo.Result
import retrofit2.http.GET
import retrofit2.http.Query


interface PhotoApi {
    @GET("photos")
    suspend fun getPhotos(
        @Query("_start") start: Int = 0,
        @Query("_limit") limit: Int = 29
    ): Result<List<PhotoResponse>>
}