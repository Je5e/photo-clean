package com.trainee.data.repository

import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.database.model.PhotoEntity
import com.trainee.data.networking.PhotoApi
import com.trainee.data.networking.model.asDatabaseModel
import com.trainee.domain.model.photo.Photo
import com.trainee.domain.model.photo.Result
import com.trainee.domain.model.photo.Result.Success
import com.trainee.domain.model.photo.Result.Error
import com.trainee.domain.model.photo.onSuccess
import com.trainee.domain.repository.PhotoRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class PhotoRepositoryImp(
    private val photoApi: PhotoApi,
    private val photoDao: PhotoDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : PhotoRepository {

    override suspend fun getAllPhotos(): Result<List<Photo>> {
        val photos = photoDao.getPhotos()
        photos?.let { return Success(photos.map { it.asDomainModel() }) }
        return Error(Exception("Photos not found"))

    }

    override suspend fun refreshPhotos() {
        withContext(ioDispatcher) {
            val photos = photoApi.getPhotos()
            photos?.onSuccess { photoList -> photoDao.insertAll(photoList.asDatabaseModel()) }
        }
    }
}