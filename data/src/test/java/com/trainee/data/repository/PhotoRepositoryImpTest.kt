package com.trainee.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.trainee.data.data.repository.FakePhotoApi
import com.trainee.data.data.repository.FakePhotoDao
import com.trainee.data.database.model.PhotoEntity
import com.trainee.data.networking.model.PhotoResponse
import com.trainee.data.networking.model.asDomainModel
import com.trainee.domain.model.photo.Result
import com.trainee.domain.model.photo.succeeded
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import okhttp3.internal.wait
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.core.IsEqual
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertFailsWith
import org.junit.jupiter.api.assertThrows

@ExperimentalCoroutinesApi
class PhotoRepositoryImpTest {

    @Test
    fun getAllPhotos_List_returnSuccededTrueAndNotNullValue() = runBlockingTest {

        // given a list of photos
        val repository = PhotoRepositoryImp(FakePhotoApi(), FakePhotoDao())

        // when requests the list of photos
        val result = repository.getAllPhotos()

        // then the list not should be null and the property succeded it must be True
        assertThat(result, (not(nullValue())))
        assertThat(result.succeeded, `is`(true))
    }

    @Test
    fun getAllPhotos_Null_ThrowsAnException() = runBlockingTest {
        // given a list of null photos
        val repository = PhotoRepositoryImp(FakePhotoApi(), FakePhotoDao(photos = null))

        // when requests that photos
        val result = repository.getAllPhotos() as Result.Error

        // then an Error exception should be thrown with the error message: "Photos not found"
        assertThat(result.exception.message, equalTo("Photos not found"))
    }


    @Test
    fun getAllPhotos_RequestsAllPhotosFromRemoteDataSource() = runBlockingTest {
        // Given remote and loca data source
        val photo1 = PhotoResponse(1, id = 1, title = "Photo1", url = "url", thumbnailUrl = "url")
        val photo2 = PhotoResponse(1, id = 2, title = "Photo2", url = "url", thumbnailUrl = "url")
        val photo3 = PhotoResponse(1, id = 3, title = "Photo3", url = "url", thumbnailUrl = "url")
        val photo4 =
            PhotoEntity(3, albumId = 1, title = "Photo1", url = "url", thumbnailUrl = "url")
        val photo5 =
            PhotoEntity(2, albumId = 1, title = "Photo2", url = "url", thumbnailUrl = "url")

        val remotePhotos = listOf(photo1, photo2).sortedBy { it.id }

        val repository = PhotoRepositoryImp(
            FakePhotoApi(remotePhotos.toMutableList()),
            FakePhotoDao(), Dispatchers.Unconfined
        )
        // When tasks are requested from the tasks repository
        repository.refreshPhotos()
        val result = repository.getAllPhotos() as Result.Success

        // Then tasks are loaded from the remote data source
        assertThat(result.data, IsEqual(remotePhotos.asDomainModel()))

    }

}