package com.trainee.data.data.repository

import com.trainee.data.networking.PhotoApi
import com.trainee.data.networking.model.PhotoResponse
import com.trainee.domain.model.photo.Photo
import com.trainee.domain.model.photo.Result
import com.trainee.domain.model.photo.Result.Error
import com.trainee.domain.model.photo.Result.Success

class FakePhotoApi(var photos: MutableList<PhotoResponse>? = mutableListOf()) : PhotoApi {
    override suspend fun getPhotos(start: Int, limit: Int): Result<List<PhotoResponse>> {
        photos?.let { return Success(ArrayList(it)) }
        return Error(
            Exception("Photos not found")
        )
    }
}