package com.trainee.data.data.repository

import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.database.model.PhotoEntity
import com.trainee.data.networking.model.PhotoResponse
import com.trainee.domain.model.photo.Result

class FakePhotoDao(var photos: MutableList<PhotoEntity>? = mutableListOf()) : PhotoDao {
    override suspend fun getPhotos(): List<PhotoEntity>? {
        return photos?.map { it }
    }

    override fun insertAll(photos: List<PhotoEntity>) {
        this.photos?.addAll(photos)
    }
}