package com.trainee.photo_clean.ui.photo

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.trainee.domain.model.photo.Photo


class PhotoDetailViewModel(
    savedStateHandle: SavedStateHandle

) : ViewModel() {

    val selectedPhoto: Photo? = savedStateHandle.get<Photo>(PHOTO_SELECTED)

    companion object {
        private const val PHOTO_SELECTED = "selectedPhoto"
    }
}