package com.trainee.photo_clean.ui.photo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.trainee.domain.model.photo.Photo
import com.trainee.photo_clean.EventObserver
import com.trainee.photo_clean.databinding.FragmentPhotoListBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class PhotoListFragment : Fragment() {

    private val viewModel by sharedViewModel<PhotoListViewModel>()

    private lateinit var binding: FragmentPhotoListBinding

    private lateinit var listAdapter: PhotoGridAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentPhotoListBinding.inflate(inflater)

        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.lifecycleOwner = this.viewLifecycleOwner
        setupListAdapter()
        setupNavigation()
    }

    private fun setupNavigation() {
        viewModel.navigateToSelectedPhotoEvent.observe(viewLifecycleOwner, EventObserver {
            openPhotoDetails(it)
        })
    }

    private fun openPhotoDetails(photo: Photo) {
        this.findNavController().navigate(
            PhotoListFragmentDirections.actionPhotoListFragmentToPhotoDetailFragment(photo)
        )
    }

    private fun setupListAdapter() {
        val viewModel = binding.viewModel
        if (viewModel != null) {
            listAdapter = PhotoGridAdapter(viewModel)
            binding.photosGrid.adapter = listAdapter
        } else {
            Log.d("ViewModel not initialized when attempting to set up adapter.","Error")
        }
    }
}