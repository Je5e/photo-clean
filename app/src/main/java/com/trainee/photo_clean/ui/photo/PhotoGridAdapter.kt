package com.trainee.photo_clean.ui.photo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.trainee.domain.model.photo.Photo
import com.trainee.photo_clean.databinding.GridViewItemBinding
import com.trainee.photo_clean.ui.photo.PhotoGridAdapter.PhotoViewHolder.Companion

class PhotoGridAdapter(private val viewModel: PhotoListViewModel) : ListAdapter<Photo,
        PhotoGridAdapter.PhotoViewHolder>(DiffCallback) {


    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val photo = getItem(position)

        holder.bind(viewModel, photo)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        return Companion.from(parent)
    }

    class PhotoViewHolder private constructor(
        private var binding:
        GridViewItemBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: PhotoListViewModel, photo: Photo) {
            binding.viewModel = viewModel
            binding.photo = photo
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): PhotoViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GridViewItemBinding.inflate(layoutInflater, parent, false)

                return PhotoViewHolder(binding)
            }
        }

    }





    companion object DiffCallback : DiffUtil.ItemCallback<Photo>() {
        override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
            return oldItem.url == newItem.url
        }
    }


}