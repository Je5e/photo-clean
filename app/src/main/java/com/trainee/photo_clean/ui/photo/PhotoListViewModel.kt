package com.trainee.photo_clean.ui.photo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trainee.domain.interaction.photo.GetAllPhotosUseCase
import com.trainee.domain.interaction.photo.RefreshPhotosUseCase
import com.trainee.domain.model.photo.Photo
import com.trainee.domain.model.photo.onSuccess
import com.trainee.photo_clean.Event
import kotlinx.coroutines.launch


class PhotoListViewModel(
    private val getAllPhotosUseCase: GetAllPhotosUseCase,
    private val refreshPhotosUseCase: RefreshPhotosUseCase
) :
    ViewModel() {

    private val _photos = MutableLiveData<List<Photo>>()
    var photos: LiveData<List<Photo>> = _photos

    private val _navigateToSelectedPhotoEvent = MutableLiveData<Event<Photo>>()
    val navigateToSelectedPhotoEvent: LiveData<Event<Photo>> = _navigateToSelectedPhotoEvent

    /**
     * Call getPhotos() on init so we can display status immediately.
     */

    init {
        getPhotosFromRepository()
        getPhotosFromCache()
    }

    private fun getPhotosFromCache() {
        viewModelScope.launch {
            getAllPhotosUseCase.invoke().onSuccess { photoList ->
                _photos.value = photoList.map { it }
            }
        }
    }

    private fun getPhotosFromRepository() {
        viewModelScope.launch {

            try {
                refreshPhotosUseCase.invoke()
            } catch (e: Exception) {

            }
        }
    }

    fun displayPhotoDetails(photo: Photo) {
        _navigateToSelectedPhotoEvent.value = Event(photo)
    }
}