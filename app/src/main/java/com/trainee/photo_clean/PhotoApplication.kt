package com.trainee.photo_clean

import android.app.Application
import androidx.databinding.library.BuildConfig
import androidx.work.Configuration
import com.trainee.data.di.databaseModule
import com.trainee.data.di.networkModule
import com.trainee.data.di.repositoryModule
import com.trainee.domain.di.interactionModule
import com.trainee.photo_clean.di.presentationModule
import dagger.hilt.android.HiltAndroidApp
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin



class PhotoApplication : Application(), Configuration.Provider {

    override fun getWorkManagerConfiguration(): Configuration =
        Configuration.Builder()
            .setMinimumLoggingLevel(if (BuildConfig.DEBUG) android.util.Log.DEBUG else android.util.Log.ERROR)
            .build()

    override fun onCreate() {
        super.onCreate()
        instance = this

        startKoin {
            androidContext(this@PhotoApplication)
            modules(appModules + domainModules + dataModules)
        }
    }
    companion object {
        lateinit var instance: Application
            private set
    }

}
val appModules = listOf(presentationModule)
val domainModules = listOf(interactionModule)
val dataModules = listOf(networkModule, repositoryModule, databaseModule)
