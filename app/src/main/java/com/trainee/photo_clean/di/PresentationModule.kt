package com.trainee.photo_clean.di

import android.os.Bundle
import androidx.lifecycle.SavedStateHandle
import com.trainee.photo_clean.ui.photo.PhotoDetailViewModel
import com.trainee.photo_clean.ui.photo.PhotoListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel { PhotoListViewModel(get(), get()) }
    viewModel { params-> PhotoDetailViewModel(params.get())}
}