package com.trainee.photo_clean.ui.photo

import android.util.Log
import com.trainee.domain.interaction.photo.GetAllPhotosUseCase
import com.trainee.domain.interaction.photo.RefreshPhotosUseCase
import com.trainee.domain.model.photo.Photo
import com.trainee.domain.model.photo.Result

class FakeGetAllPhotoUseCaseImp:GetAllPhotosUseCase {
    override suspend fun invoke(): Result<List<Photo>> {
        return Result.Success(listOf<Photo>())
    }
}
class FakeRrefreshPhotoUseCaseImp:RefreshPhotosUseCase {
    override suspend fun invoke() {
        Log.d("FAKE USE CASE","TEST")
    }

}