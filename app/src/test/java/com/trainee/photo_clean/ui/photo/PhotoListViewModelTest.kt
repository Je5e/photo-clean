package com.trainee.photo_clean.ui.photo
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.trainee.domain.model.photo.Photo
import com.trainee.photo_clean.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class PhotoListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun displayPhotoDetails_setNavigateToSelectedPhotoEvent() {
        //  given a fresh viewModel
        val viewModel = PhotoListViewModel(
            FakeGetAllPhotoUseCaseImp(),
            FakeRrefreshPhotoUseCaseImp()
        )

        //  when navigating to a photo
        viewModel.displayPhotoDetails(
            Photo(
                albumId = 1, id = 1, title = "title",
                url = "url", thumbnailUrl = "thumbnailurl"
            )
        )

        // then the navigate to photo detail event is triggered
        val value = viewModel.navigateToSelectedPhotoEvent.getOrAwaitValue()
        assertThat(value.getContentIfNotHandled(), (not(nullValue())))
    }
}